const express = require('express');
const Dealer = require('../domain/dealer');
const router = express.Router();

router.get('/api/dealer/action', function (req,res,next) {
  const dealer = new Dealer();
  const result = {'can hit': dealer.canHit(parseCards(req.query.card)) };
  res.send(result);
});

function parseCards(cards) {
  if(Array.isArray(cards)) {
    return cards
  }
  return [cards];
}

module.exports = router;
