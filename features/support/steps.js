const { Given, When, Then } = require("cucumber");
const assert = require("assert").strict

Given(/^у дилера на руках "([^"]*)"$/, function (card) {
    this.addCard(card);
});

When(/^он берет "([^"]*)" из колоды$/, function (card) {
    this.addCard(card);
});

Then(/^он не должен брать еще карту$/, function () {
    return this.dealerCanHit().then(
        res => {
            assert.equal(res['can hit'], false);
        }
    );
});

Then(/^он должен взять еще карту$/, function () {
    return this.dealerCanHit().then(
        res => {
            assert.equal(res['can hit'], true);
        }
    );
});