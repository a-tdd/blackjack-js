const fetch = require('node-fetch');

const {setWorldConstructor} = require("cucumber");

async function getDataFromAPI(url) {
    console.log("url: "+url);
    let response = await fetch(url)
    let data = await response.json()
    return data;
}

class CustomWorld {

    constructor() {
        this.cards = [];
    }

    addCard(card) {
        this.cards.push(card);
    }


    composeUrl() {
        let url = "http://localhost:3000/api/dealer/action";
        if (this.cards.length > 0) {
            const params = this.cards.map(x => "card=" + x).join("&")
            url += "?" + params;
        }
        return url;
    }

    dealerCanHit() {
        return getDataFromAPI(this.composeUrl())
    }
}

setWorldConstructor(CustomWorld);