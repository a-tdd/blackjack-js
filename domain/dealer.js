const cards = {
    'Q': 10,
    '6': 6,
    '7': 7
}

function getValue(card) {
    return cards[card];
}

class Dealer {
    canHit(hand) {
        console.log("cards on hand: "+ hand);
        const value = hand
            .map(card => getValue(card))
            .reduce((acc, i) => acc + i)

        if (isNaN(value)) {
            throw Error("can't calculate value for cards: " + hand);
        }

        return value < 17
    }
}

module.exports = Dealer;